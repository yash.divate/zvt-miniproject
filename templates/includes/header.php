<!-- header -->
<?php
 $rootpath = $config->urls->templates; 
// echo $roothpath;
// echo "1";
// echo "1";

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>project</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"><!-- bootstrap includes -->
  <link rel="stylesheet" href="<?=$rootpath?>styles/header.css"><!-- custom footer css file -->
  <link rel="stylesheet" href="<?=$rootpath?>styles/footer.css"> <!--  custom footer css file -->
  <link rel="stylesheet" href="<?=$rootpath?>styles/home.css"> <!--  custom footer css file -->
  <link rel="stylesheet" href="<?=$rootpath?>styles/aboutus.css"> <!--  custom footer css file -->
</head>
<body>

  <nav class="navbar navbar-expand-lg">
      <div class="container">
          <a class="navbar-brand" href="<?=$pages->get("name=home")->httpUrl;?>">
            <h1 class="logo_txt">`SneaKers`</h1>
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse text-center" id="navbarNavAltMarkup">
          <div class="navbar-nav ms-auto">
              <a class="nav-link p-2 m-2" aria-current="page" href="<?=$pages->get("name=home")->httpUrl;?>">Home</a>
              <a class="nav-link p-2 m-2" href="<?=$pages->get("name=home")->httpUrl;?>#p-card">Products</a>
              <a class="nav-link p-2 m-2" href="<?=$pages->get("name=about-us")->httpUrl;?>">About</a>
          </div>
          </div>
      </div>
  </nav>
<!-- END OF HEADER -->