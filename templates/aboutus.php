<?php include './includes/header.php';?><!-- header file -->

<div class="container">
  <div class="row">
    <div class="col-lg-8">
      <div class="t_title text-center mt-4">
        <h1 style="color:black;"><?=$page->headline;?></h1>
      </div>
      <p style="color:black;"><?=$page->body?></p>
    </div>
    <div class="col-lg-4 overflow-hidden left-card mt-4 ">
      <img src="<?=$page->headimage->first()->httpUrl;?>" alt="aboutus image">
    </div>
  </div>
</div>  

<?php include './includes/footer.php';?><!-- header file -->