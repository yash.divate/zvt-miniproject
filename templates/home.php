<?php namespace ProcessWire;

// Template file for “home” template used by the homepage
?>

<?php include './includes/header.php';?><!-- header file -->

<style>
    html,body{
        /* height: 200vh; */
    }
</style>
<!-- carousel -->
<section id="car-slide">
  
 <div id="carouselExampleFade" class="carousel slide carousel-fade carousel-img">

  <div class="carousel-inner ">   <!-- loop using processwire -->
    <?php
    $counter=0;            // init counter to 0 
    foreach ($page->repeater as $repeater){        //access the repeater using for eachloop  
        if($counter==0){                           //counter equal to 0 add active class
          $active='active';
        }else{
          $active='';                               //esle remove active class
        }
        ?>
        <div class="carousel-item <?=$active?>">        <!--  if active show img -->
          <img class="img-fluid" src="<?php echo $repeater->banner_img->url;?>">
        </div>
      
    
      <?php 
      $counter++;
    }?>
    </div>

  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div> 
</section>


<!-- cards -->
<section id="p-card">
  <div class="container">
      <div class="row justify-content-center align-items-center">
          <?php foreach($pages->get("name=products")->children() as $product_card):?> 
            <div class="col-lg-4 col-md-6 d-flex justify-content-center align-center">
                <div class="card mt-4">       
                  <div class="card_img img-fluid">
                    <a href="<?=$product_card->httpUrl;?>">
                      <img src="<?=$product_card->headimage->first()->httpUrl?>">                   
                    </a>                   
                  </div>     
                  <div class="card-body">
                        <strong><h5 style="color:black;" class="card-title text-center fw-lighter fs-6"><?= $product_card->headline;?></h5>
                        <p style="color:#339F62;" class="card-text text-center fs-5"><?=$product_card->body;?></p></strong>
                        <p style="color:#339F62;" class="card-price text-center fs-6 "><?=$product_card->card_price;?></p>
                        <!-- <a href="#" class="btn btn-primary w-100">ADD TO CART</a>-->
                  </div>
                </div>
              </div>
              <?php endforeach;?>
        </div>
  
  </div>
</section>

<!-- service banner -->
<!-- <div class="container-fluid my-5"> 
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 overflow-hidden text-center">
      <div class="icon_banner">
        <img src="site\assests\shipping.png" alt="shipping-img">
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 overflow-hidden text-center">
      <div class="icon_banner">
        <img src="site\assests\gift.png" alt="shipping-img">
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 overflow-hidden text-center">
      <div class="icon_banner">
        <img src="site\assests\contact.png" alt="shipping-img">
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 overflow-hidden text-center">
      <div class="icon_banner">
        <img src="site\assests\return.png" alt="shipping-img">
      </div>
    </div>
  </div>
</div> -->

<?php include './includes/footer.php';?><!-- footer file -->



