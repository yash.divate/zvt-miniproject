<?php
namespace ProcessWire;

// Check if the "submit" button is clicked (submitted from the main form)
if (isset($_POST['submit'])) {
    // Access the form data through POST method
    $fname = $sanitizer->text($_POST['firstname']);
    $lname = $sanitizer->text($_POST['lastname']);
    $number = $sanitizer->text($_POST['number']);
    $address = $sanitizer->text($_POST['address']);

    // Create a new ProcessWire page
    $formpage = new Page();
    $formpage->template = $templates->get("form");
    $formpage->parent = $pages->get("name=regform");
    $formpage->title = $fname . " " . $lname;
    $formpage->save();

    // Storing form data in ProcessWire fields
    $formpage->firstname = $fname;
    $formpage->lastname = $lname;
    $formpage->address = $address;
    $formpage->number = $number;

    // Turn off output formatting before saving
    $formpage->of(false);
    $formpage->save();
    echo "Form submitted";
    // Turn on output formatting again
    $formpage->of(true);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>

<div class="container mt-5">
    <h1 class="text-center">Form</h1>
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <form action="" method="post">
                <div class="mb-3">
                    <label for="firstname" class="form-label">Firstname</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" required>
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Lastname</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" required>
                </div>
                <div class="mb-3">
                    <label for="number" class="form-label">Phone number</label>
                    <input type="tel" class="form-control" id="number" name="number" required>
                </div>
                <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" class="form-control" id="address" name="address" required>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary w-50 mb-5" name="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Display form data -->
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">sr.no</th>
            <th scope="col">Firstname</th>
            <th scope="col">Lastname</th>
            <th scope="col">Number</th>
            <th scope="col">Address</th>
            <th scope="col">Delete</th>
            <th scope="col">Update</th>
        </tr>
        </thead>
        <?php
        $counter = 1;
        foreach ($pages->get("name=regform")->children() as $regdata):
            ?>
            <tbody>
            <tr>
                <th scope="row"><?= $counter ?></th>
                <td><?= $regdata->firstname ?></td>
                <td><?= $regdata->lastname ?></td>
                <td><?= $regdata->number ?></td>
                <td><?= $regdata->address ?></td>
                <td>
                    <form action="" method="post">
                        <!-- Use the correct form field name -->
                        <input type="hidden" name="delete_row_id" value="<?= $regdata->id ?>">
                        <button type="submit" class="btn btn-danger" name="delete">DELETE</button>
                    </form>
                </td>
                <td>
                    <form action="" method="post">
                        <!-- Use the correct form field name -->
                        <input type="hidden" name="edit_row_id" value="<?= $regdata->id ?>">
                        <button type="submit" class="btn btn-info" name="edit">EDIT</button>
                    </form>
                </td>
            </tr>
            </tbody>
            <?php
            $counter++;
        endforeach;
        ?>

        <!-- Deleting the data from the table -->
        <?php
        if (isset($_POST['delete'])) {
            $rowToDelete = (int)$_POST['delete_row_id'];
            $targetPage = $pages->get("id=$rowToDelete"); // Use 'id' to identify the page

            if ($targetPage->id) {
                $targetPage->delete();
                // Redirect or display a success message
                // You can use JavaScript to refresh the page or update the table without a full page reload.
                echo "<script>window.location.reload();</script>";
            } else {
                echo "NOT FOUND";
            }
        }
        ?>

        <!-- Edit code button start -->
        <?php

      if (isset($_POST['edit_row_id'])) {
          $rowToEdit = (int)$_POST['edit_row_id'];
          $targetPage = $pages->get("id=$rowToEdit");
          $targetPage->of(false);
          
          if ($targetPage->id) {
              $editedFirstName = $targetPage->firstname;
              $editedLastName = $targetPage->lastname;
              $editedNumber = $targetPage->number;
              $editedAddress = $targetPage->address;
              
          } else {
              echo "Page not found";
          }
      }
      
      // Check if the "save" button is clicked (submitted from the edit form)
      if (isset($_POST['save'])) {
          // Get the ID of the row to update
          $rowToEdit = (int)$_POST['edit_row_id'];
          
          // Get the target page from ProcessWire using the ID
          $targetPage = $pages->get("id=$rowToEdit");
      
          if ($targetPage->id) {
              //turn off output formatting for the edit part 
              $targetPage->of(false);
              // Update the page fields with the edited data
              $targetPage->firstname = $sanitizer->text($_POST['edited_firstname']);
              $targetPage->lastname = $sanitizer->text($_POST['edited_lastname']);
              $targetPage->number = $sanitizer->text($_POST['edited_number']);
              $targetPage->address = $sanitizer->text($_POST['edited_address']);
              
              // Save the updated page
              $targetPage->save();
              // Turn output formatting back on for the target page
              $targetPage->of(true);
              // Redirect back to the main page or display a success message
              // You can use JavaScript to redirect or show a success message here
            } else {
              echo "Page not found";
            }
            echo "<script>window.location.reload();</script>";
        }
        ?>
    </table>
</div>
<div class="container mt-5">
              <div class="row">
                  <div class="col-lg-6 offset-lg-3">
                      <!-- Display the edit form when "Edit" is clicked -->
                      <?php if (isset($_POST['edit_row_id'])): ?>
                        <h1 class="text-center mt-5 p-5">Edit form</h1>
                          <form action="" method="post">
                              <!-- Display the data from the selected record -->
                              <div class="mb-3">
                                  <label for="edited_firstname" class="form-label">Edited Firstname</label>
                                  <input type="text" class="form-control" id="edited_firstname" name="edited_firstname" value="<?= $editedFirstName ?>" required>
                              </div>
                              <div class="mb-3">
                                  <label for="edited_lastname" class="form-label">Edited Lastname</label>
                                  <input type="text" class="form-control" id="edited_lastname" name="edited_lastname" value="<?= $editedLastName ?>" required>
                              </div>
                              <div class="mb-3">
                                  <label for="edited_number" class="form-label">Edited Number</label>
                                  <input type="text" class="form-control" id="edited_number" name="edited_number" value="<?= $editedNumber ?>" required>
                              </div>
                              <div class="mb-3">
                                  <label for="edited_address" class="form-label">Edited Address</label>
                                  <input type="text" class="form-control" id="edited_address" name="edited_address" value="<?= $editedAddress ?>" required>
                              </div>
                             
                              <div class="text-center">
                                  <input type="hidden" name="edit_row_id" value="<?= $rowToEdit ?>">
                                  <button type="submit" class="btn btn-success w-50 text-center mb-5" name="save">Save</button>
                              </div>
                          </form>
                      <?php endif; ?>
                      <!-- <script>window.location.reload();</script> -->
                      <!-- error in the reload function in java script -->

                  </div>
              </div>
          </div>
      

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>
</html>
