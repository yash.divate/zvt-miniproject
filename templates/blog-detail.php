<?php include './includes/header.php';?><!-- header file -->

<!-- detail product -->
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="product_img  d-flex justify-content-center align-item-center">
                <img  class="main_img" src="<?=$page->headimage->first()->httpUrl;?>" alt="P-img">
            </div>
            <div class="detail_img_products text-center">
                <?php foreach ($page->detail_product_img as $img):?>          
                    <img id="d_P_img" class="w-25 img_id" src="<?=$img->httpUrl;?>" alt="">
                <?php endforeach?>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="product_desc mt-5">
                <div class="p_title">
                    <h4 style="color: black;;"><?=$page->headline;?></h4>
                    <h1 style="color:#339F62;"><?=$page->body?></h1>
                </div>

                <div class="p_info">
                    <p style="color:black;"><?=$page->product_desc?></p>
                    <p style="color:black;"><?=$page->product_desc?></p>

                </div>
                <hr>
                <div class="p_price">
                    <h2 style="color:black;"><?=$page->card_price?>/-</h2>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- recent pages -->
<div class="container">
    <div class="row">
        <div class="latest-card mt-5 text-center">
            <h3 style="color:black;">YOU MAY ALSO LIKE</h3>
        </div>
    </div>
</div>

<!-- card-section -->
<section id="p-card">

  <div class="container mt-5">
      <div class="row justify-content-center align-items-center">
          <?php foreach($pages->get("name=products")->children("limit=3, sort=random") as $product_card):?> 
            <div class="col-lg-4 col-md-6 d-flex justify-content-center align-center">
                <div class="card mb-5 mt-3">       
                  <div class="card_img img-fluid">
                    <a href="<?=$product_card->httpUrl;?>">
                      <img src= <?=$product_card->headimage->first()->httpUrl?>>                   
                    </a>                   
                  </div>     
                  <div class="card-body">
                        <strong><h5 style="color:#339F62;" class="card-title text-center fw-lighter fs-6"><?= $product_card->headline;?></h5>
                        <p style="color:#339F62;" class="card-text text-center fs-5"><?= $product_card->body;?></p></strong>
                        <p class="card-price text-center fs-6 "><?= $product_card->card_price;?></p>
                        <!-- <a href="#" class="btn btn-primary w-100">ADD TO CART</a>-->
                  </div>
                </div>
              </div>
              <?php endforeach;?>
        </div>
  
  </div>
</section>

<?= include './includes/footer.php';?>